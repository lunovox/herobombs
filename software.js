$pass = [];

if(
	typeof($tentativas) == "undefined" || 
	$tentativas<=0
){
	$tentativas = 15;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
function getUrlVar($var) {
	return getUrlVars()[$var];
}

function setPassword($newpass) {
	if(typeof $newpass == "undefined" || $newpass==null || $pass.length==0 ){
		$pass = [];
		$pass[0] = Math.floor(Math.random() * 9); 
		$pass[1] = Math.floor(Math.random() * 9); 
		$pass[2] = Math.floor(Math.random() * 9); 
		$pass[3] = Math.floor(Math.random() * 9); 
		while (
			$pass[0]==$pass[1] || $pass[0]==$pass[2] ||  $pass[0]==$pass[3] || 
			$pass[1]==$pass[2] || $pass[1]==$pass[3] || 
			$pass[2]==$pass[3]
		) {
			$pass[0] = Math.floor(Math.random() * 9); 
			$pass[1] = Math.floor(Math.random() * 9); 
			$pass[2] = Math.floor(Math.random() * 9); 
			$pass[3] = Math.floor(Math.random() * 9); 
		}
		//console.log("setPassword([" + $pass[0] +","+ $pass[1] +","+ $pass[2] +","+ $pass[3] +"]);");
	}else{
		//console.log("setPassword([" + typeof($newpass)+"]);");
		$pass = $newpass
	}
}/**/

function criarPalco($fase, $next) {
	if(typeof $pass == "undefined" || $pass==null || $pass.length==0){
		setPassword(null);
	}
	//console.log("setPassword([" + $pass[0] +","+ $pass[1] +","+ $pass[2] +","+ $pass[3] +"]);");
	
	var $tries = document.getElementById("tries");
	//$tries.style.display = "inline-block";
	//$tries.style.borderStyle = "solid";
	//$tries.style.borderWidth = "thick";
	$tries.className ="divBlockLarg"; //Classe CSS
	$tries.style.backgroundColor = "rgba(0,0,0,0.3)";
	$tries.style.padding="5px 5px 5px 5px";
	$tries.align = "center";
	var $try = [];
	for($t=1;$t<=$tentativas;$t++){
		//print("try"+$t);
		$try[$t-1] = document.createElement('div');
		$try[$t-1].id = "try"+$t;
		if($try[$t-1]!=null){
			//$try[$t-1].style="font-family:verdana;";
			$try[$t-1].style.fontFamily="verdana,Impact,Charcoal,sans-serif";
			$try[$t-1].style.fontSize="13";
			
			for($c=1;$c<=4;$c++){
				var $sel = document.createElement('select');
				$sel.id = "char_"+$t+"_"+$c;
				$sel.disabled = $t!=1;false;true;
				for($i=0;$i<=9;$i++){
					var newOpt = document.createElement('option');
					newOpt.text = $i
					newOpt.value = $i
					$sel.add(newOpt);
				}
				$try[$t-1].appendChild($sel);
			}
								
			$try[$t-1].innerHTML +=" ";
			//$try[$t-1].innerHTML +=" A:";
			var $input = document.createElement('input');
			$input.id = "acertos_"+$t;
			$input.title = "Quantidade de algarismos acertados!!!";
			$input.type = "text";
			$input.className ="txthint"; //Classe CSS
			$input.disabled = true
			$try[$t-1].appendChild($input);
			
			//$try[$t-1].innerHTML +=" P:";
			var $input = document.createElement('input');
			$input.id = "posicoes_"+$t;
			$input.title = "Quantidade de algarismos que estão na posição correta!!!";
			$input.type = "text";
			$input.className ="txthint"; //Classe CSS
			$input.disabled = true
			$try[$t-1].appendChild($input);
			
			$try[$t-1].innerHTML +=" ";
			var $button = document.createElement('button');
			$button.id = "test_"+$t;
			$button.test = $t;
			$button.innerHTML = "!";
			$button.title = "Testar senha!!!";
			$button.disabled = $t!=1;false;true;
			$button.onclick = function(){
				var $t = this.test;
				//console.log("button[test_"+$t+"].onclick()");
				var $charac = [];
							
				for($c=1;$c<=4;$c++){
					//console.log("char_"+$t+"_"+$c);
					$charac[$c-1] = document.getElementById("char_"+$t+"_"+$c);
				}
				if($charac[0]!=null){
					if($charac[1]!=null){
						if($charac[2]!=null){
							if($charac[3]!=null){
								if(
									$charac[0].value!=$charac[1].value && $charac[0].value!=$charac[2].value && $charac[0].value!=$charac[3].value && 
									$charac[1].value!=$charac[2].value && $charac[1].value!=$charac[3].value && 
									$charac[2].value!=$charac[3].value
								){
									//alert("Ok!")
									this.disabled = true;
									$txtAcertos = document.getElementById("acertos_"+$t);
									$txtPosicionados = document.getElementById("posicoes_"+$t);
									
									var countAcertos = 0;
									var countPosicionados = 0;
									for($c1=1;$c1<=4;$c1++){
										$charac[$c1-1].disabled = true;
										var nextChar = document.getElementById("char_"+($t+1)+"_"+$c1);
										if(nextChar!=null){nextChar.disabled = false;}
										
										for($c2=1;$c2<=4;$c2++){
											if($charac[$c1-1].value == $pass[$c2-1]){
												countAcertos++;
												if($c1==$c2){
													countPosicionados++;
												}
											}
										}
									}
									if(countAcertos==4 && countPosicionados==4){
										window.location = "youwin.html?f="+$fase+"&n="+$next+"&p="+$pass[0] + $pass[1] + $pass[2] + $pass[3];
									}else{
										$txtAcertos.value = countAcertos;
										$txtPosicionados.value = countPosicionados;
										var nextBtnTest = document.getElementById("test_"+($t+1));
										if(nextBtnTest!=null){
											nextBtnTest.disabled = false;
										}else{
											window.location = "youloser.html?f="+$fase;
										}
									}
								}else{
									alert("Não deve existir simbolos repetidos nesta senha!")
								}
							}else{
								alert("Erro: $charac[3] não encontrado!")
							}
						}else{
							alert("Erro: $charac[2] não encontrado!")
						}
					}else{
						alert("Erro: $charac[1] não encontrado!")
					}
				}else{
					alert("Erro: $charac[0] não encontrado!")
				}
			};
			$try[$t-1].appendChild($button);
		}
		$tries.appendChild($try[$t-1]);
	}
	//console.log ($tries.innerText);
}
